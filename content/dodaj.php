<?php

if ($_GET['typ_kor'] == 'o') {
	$typy = array('odebrana', 'Nadawca', 'nadawcę');
	$typ_kor = $_GET['typ_kor'];
} else {
	$typy = array('wysłana', 'Odbiorca', 'odbiorcę');
	$typ_kor = $_GET['typ_kor'];
}
if (isset($_GET['nr']) && $_GET['nr'] != 0) {
	//echo '<div id="komunikat">';
	//echo "Wpis nr <span style=\"font-weight:bold\">{$_GET['nr']}</span> został dodany do bazy.";
	echo "<script>document.getElementById('komunikat').innerHTML='Wpis nr {$_GET['nr']} został dodany do dziennika.';</script>";
	//echo '</div>';
} elseif (isset($_GET['0'])) { //indeksy 0, 1, 2 związane są z błędem
	/*echo '<div id="komunikat">';
	echo "\n<span style=\"color:red\">Wystąpił błąd!</span><br/>Skontaktuj się z administratorem, podając poniższy komunikat:<br/>";
	echo '<span style="font-family:sans-serif">' . $_GET['1'] . ' (' . $_GET['0'] . '): ' . $_GET['2'] . '</span>';
	echo "\n</div>";*/
	$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść poniższego komunikatu:\n';
	$sql_kom .= $_GET['1'] . ' (' . $_GET['0'] . '):\n' . $_GET['2'];
	echo '<script>alert("' . $sql_kom . '");window.location.assign("?strona=dodaj&typ_kor=' . $_GET['typ_kor'] . '")</script>';
}

if (isset($_POST['adrDodNazwa'])) {
	echo "<script>document.getElementById('komunikat').innerHTML='Adresat \'{$_POST['adrDodNazwa']}\' został dodany do dziennika.';</script>";
}
?>
<form method="post" id="dodaj" name="dodaj" onSubmit="return czyPuste('<?php echo $typy[1]; ?>');"></form>
<form method="post" id="dodaj_rek"> <!--dodaje rekord do bazy - tu: rekord książki adresowej-->
	<input type="hidden" name="adrDodNazwa" id="adrDodNazwa"/> 
</form>
<table id="korespondencja">
	<thead>
		<tr>
			<th style="text-align:left; border-bottom:2px dotted; padding-top:10px">Korespondencja <?php echo $typy[0]; ?></th>
		</tr>
	</thead>		
	<tr>
		<td>Data<br/>
			<input type="text" name="data[]" style="width:19px" maxlength="2" value="<?php echo date('d');?>" form="dodaj"/> -
			<input type="text" name="data[]" style="width:19px" maxlength="2" value="<?php echo date('m');?>" form="dodaj"/> -
			<input type="text" name="data[]" style="width:35px" maxlength="4" value="<?php echo date('o');?>" form="dodaj"/>
		</td>
	</tr>
	<tr>
		<td><?php echo $typy[1]; ?><br/>
			<select name="nad_odb" size="6" style="width:100%" id="<?php echo $typy[1]; ?>"  form="dodaj">
				<option value="" selected>---</option>
<?php
//Pobranie z bazy listy nadawców/odbiorców zdefiniowanych
$wynik = $pol->query("SELECT nr, nazwa FROM " . sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci" ." ORDER BY nazwa", PDO::FETCH_NUM);
foreach($wynik as $wiersz) {
	echo "\t\t\t\t<option value=\"$wiersz[0]\">$wiersz[1]</option>\n";
}
?>
			</select>
		</td>
	</tr>
	<tr>
		<td style="text-align:right;padding:0px">
			<button onClick="popupwindow('content/dodaj_adresata.php', 600, 450)">Dodaj nowego <?php echo $typy[2]; ?></button>
		</td>
	</tr>
	<tr>
		<td>Treść korespondencji<br/><textarea rows="3" style="width:100%" name="tresc" form="dodaj"></textarea></td>
	</tr>
	<tr>
		<td>Uwagi<br/><textarea rows="3" style="width:100%" name="uwagi" form="dodaj"></textarea></td>
	</tr>
	<tr style="text-align:center">
		<td style="padding-top:20px"><input type="submit" style="font-size:16px;width:90px;height:30px;" value="Zapisz" name="php" form="dodaj"/></td>
	</tr>
</table>
<?php 
if (isset($_POST['php'])) {
	extract($_POST); //Tworzy zmienne: $data,$nad_odb,$tresc,$uwagi
	$data = implode('-', array_reverse($data)); //Robi string 0000-00-00 z tablicy z datą
	$zapytanie = $pol->prepare("INSERT INTO " . sprintf("%02d", $_SESSION['id_placowki']) . "_korespondencja" ." (data, typ_kor, nad_odb, tresc, uwagi) VALUES (?, ?, ?, ?, ?)");
	$wynik = $zapytanie->execute(array($data,$typ_kor,$nad_odb,$tresc,$uwagi));
	if ($zapytanie->errorCode() !== '00000') { //errorCode jest zawsze stringiem, więc można porównać typy (stąd !== zamiast !=)
		$errorInfo = $zapytanie->errorInfo();
		$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść\nponiższego komunikatu:\n';
		$sql_kom .= $errorInfo[1] . ' (' . $errorInfo[0] . '):\n' . $errorInfo[2];
		echo '<script>alert("' . $sql_kom . '");</script>';
	} else {
		$adrPrzekier = "index.php?strona=dodaj&typ_kor={$_GET['typ_kor']}&nr={$pol->lastInsertId()}";
		echo "<script>location.href='" . filter_var($adrPrzekier, FILTER_SANITIZE_URL) . "'</script>";
	}
	/*
	//Przekierowanie w celu eliminacji problemu z repostami:
	$przekieruj =  "index.php?strona=dodaj&typ_kor={$_GET['typ_kor']}";
	if (isset($sql_blad)) {
	$przekieruj .= '&' . http_build_query($sql_blad);
	} else {
	$przekieruj .= "&nr={$pol->lastInsertId()}";
	}
	//$_SESSION['i']++;
	*/
	
}
?>

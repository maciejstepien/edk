<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Dodawanie adresata</title>
	<link type="text/css" rel="stylesheet" href="../css/okienko.css"/>
	<script type="text/javascript" src="../js/skrypty.js"></script>
</head>
<body>
<?php
//Zabiezpieczenie przed dostępem do okienka bez uwierzytelnienia (zalogowania)
if (!isset($_SESSION['token'])) {
	echo "Aby dodać adresata, musisz być zalogowany.";
} else {
?>
	<form method="post" id="dod_adr" onSubmit="return czyPuste('Nazwa')"></form>
	<p style="text-align:center">Dodawanie nowego adresata (nadawcy/odbiorcy)</p><br/>
	<table class="okienkowa" id="dod_adr">
		<tr>
			<td>Nazwa*</td>
			<td><input type="text" name="nazwa" id="Nazwa" style="width:100%" form="dod_adr"/></td>
		</tr>
		<tr>
			<td>Miejscowość</td>
			<td><input type="text" name="miejsc" style="width:100%" form="dod_adr"/></td>
		</tr>
		<tr>
			<td>Ulica i numer</td>
			<td><input type="text" name="ulica" style="width:100%" form="dod_adr"/></td>
		</tr>
		<tr>
			<td>Kod</td>
			<td><input type="text" name="kod" size="6" maxlength="6" form="dod_adr"/></td>
		</tr>
		<tr>
			<td>Telefon</td>
			<td><input type="text" name="tel" style="width:100%" form="dod_adr"/></td>
		</tr>
		<tr>
			<td>e-mail</td>
			<td><input type="text" name="mail" style="width:100%" form="dod_adr"/></td>
		</tr>
		<tr>
			<td>Informacje dodatkowe</td>
			<td><input type="text" name="dod_info" style="width:100%" form="dod_adr"/></td>
		</tr>
		<tr>
			<td colspan="2" style="text-align:center"><input type="submit" value="Zapisz" form="dod_adr"/></td>
		</tr>
	</table>
	<br/>
	<p style="font-size:10px;text-align:center">Pola oznaczone gwiazdką (*) są obowiązkowe.</p>
	<input type="checkbox" name="czyZamknac" value="1" checked form="dod_adr"/><span style="font-size:11px;">Zamknij to okno po dodaniu adresata.</span>
<?php
	if (isset($_POST['nazwa']) && $_POST['nazwa'] != '') {
		extract($_POST); //tworzy zmienne $nazwa, $miejsc itd.
		//Zamiast czystego extract($_POST) daję to, by wyeliminowac tagi html
		/*foreach ($_POST as $klucz => $wartosc) {
			$$klucz = strip_tags($wartosc);
		}*/
		//$kod = implode('-', $kod);
		//Nawiązanie połączenia z bazą MySQL
		require "../config/db.php"; //dane logowania do bazy MySQL
		try {
			$pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
			$pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
		} catch (PDOException $e) {
			echo 'Połączenie nieudane: ' . $e->getMessage();
			exit;
		}
                $pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
		//Dodawanie adresata do bazy
		$zapytanie = $pol->prepare("INSERT INTO " . sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci" . " (nazwa, miejsc, ulica, kod, tel, mail, dod_info) VALUES (?, ?, ?, ?, ?, ?, ?)");
		$zapytanie->execute(array($nazwa, $miejsc, $ulica, $kod, $tel, $mail, $dod_info));
		//Sprawdzanie błędów i powrót do głównego okna
		if ($zapytanie->errorCode() != '00000') {
			if ($zapytanie->errorCode() == '23000') {
				$sql_kom = "Adresat '" . addslashes($nazwa) . "' już istnieje. Użyj innej nazwy lub edytuj dane istniejącego adresata.";
			} else {
				$errorInfo = $zapytanie->errorInfo();
				$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść poniższego komunikatu:\n';
				$sql_kom .= addslashes($errorInfo[1] . ' (' . $errorInfo[0] . '):') . '\n' . addslashes($errorInfo[2]);
			}
			echo "<script>alert(\"$sql_kom\");</script>";
		} else {
			//echo "<script>window.opener.location.search='?strona=ksiazka&adrNazwaDod=$nazwa';window.close();</script>";
			if (isset($_POST['czyZamknac'])) {
				$zamknijOkno = 'window.close();';
			} else {
				$zamknijOkno = '';
				$checked = '';
			}
			echo "<script>opener.document.getElementById('adrDodNazwa').value='" . addslashes($nazwa) . "';";
			echo "opener.document.getElementById('dodaj_rek').submit();$zamknijOkno</script>";
		}
	}
} //zakończenie warunku zalogowania
?>

</body>
</html>

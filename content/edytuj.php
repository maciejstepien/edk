<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Edycja rekordu</title>
	<link type="text/css" rel="stylesheet" href="../css/okienko.css"/>
</head>
<body>
<form method="post" id="ed_rek"></form>
<?php
if (!isset($_SESSION['token'])) die("Musisz być zalogowany, by móc edytować wpis dziennika.\n</body>\n</html>");
//Nawiązanie połączenia z bazą MySQL
require "../config/db.php"; //dane logowania do bazy MySQL
try {
	$pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
} catch (PDOException $e) {
    echo 'Połączenie nieudane: ' . $e->getMessage();
    exit;
}
$pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
if (isset($_POST['czy'])) {
	extract($_POST); //Tworzy zmienne: $data,$nad_odb,$tresc,$uwagi
	$data = implode('-', array_reverse($data)); //Robi string 0000-00-00 z tablicy z datą
	//Edycja adresata w bazie
	$zapytanie = $pol->prepare("UPDATE " .sprintf("%02d", $_SESSION['id_placowki']) . "_korespondencja " . "
								SET	data=?, typ_kor=?, nad_odb=?, tresc=?, uwagi=?
								WHERE nr=?");
	$zapytanie->execute(array($data,$typ_kor,$nad_odb,$tresc,$uwagi,$_GET['nr']));
	//Obsługa błędów
	if ($zapytanie->errorCode() != '00000') {
		$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść poniższego komunikatu:\n';
		$sql_kom .= implode('\n', $zapytanie->errorInfo());
		echo '<script>alert("' . $sql_kom . '");</script>';
	} else {
		echo "<script>opener.document.getElementById('wpisEdNr').value='{$_GET['nr']}';
					opener.document.getElementById('edytuj_rek').submit();window.close();</script>";
	}
	//echo $sql_kom;
	//die("<script>window.opener.location.reload();</script>");
}

$wynik_adr_wsz = $pol->query("SELECT nr, nazwa 
								FROM " .sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci " . "
								ORDER BY nazwa ASC", PDO::FETCH_NUM);
$wynik_ed = $pol->prepare("SELECT * FROM " .sprintf("%02d", $_SESSION['id_placowki']) . "_korespondencja " . "
							WHERE nr = ?");
$wynik_ed->execute(array($_GET['nr']));
$wynik_ed->setFetchMode(PDO::FETCH_NUM);
foreach($wynik_ed as $wiersz) {
	//pobranie nazwy adresata z książki adresowej
	$adresat = $pol->query("SELECT nazwa 
								FROM " .sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci " . "
								WHERE nr = $wiersz[3]")->fetch(PDO::FETCH_NUM);
	echo "<p style=\"text-align:center\">Edycja rekordu nr $wiersz[0]</p><br/>";
	echo "<table class=\"okienkowa\">";
	$data = explode('-', $wiersz[1]);
	echo "<tr><td>Data</td><td>
	<input type=\"number\" min=\"1\" max=\"31\" style=\"width:40px\" name=\"data[]\" value=\"$data[2]\" form=\"ed_rek\"/> -
	<input type=\"number\" min=\"1\" max=\"12\" style=\"width:40px\" name=\"data[]\" value=\"$data[1]\" form=\"ed_rek\"/> -
	<input type=\"number\" min=\"2014\" max=\"2030\" style=\"width:60px\" name=\"data[]\" value=\"$data[0]\" form=\"ed_rek\"/></td></tr>";
	echo "<tr><td>Typ korespondencji</td><td><select name=\"typ_kor\" form=\"ed_rek\" />";
	if ($wiersz[2] == 'o') {
		echo "<option value=\"o\" selected>odebrane (o)</option><option value=\"w\">wysłane (w)</option>";
	} else {
		echo "<option value=\"o\">odebrane (o)</option><option value=\"w\" selected>wysłane (w)</option>";
	}
	echo "</select></td></tr>";
	echo "<tr><td>Nadawca/odbiorca</td><td><select name=\"nad_odb\" form=\"ed_rek\" style=\"width:100%\">";
	echo "<option value=\"$wiersz[3]\" selected>$adresat[0]</option>";
	foreach($wynik_adr_wsz as $adresaci) { 
		if ($adresaci[0] != $wiersz[3]) {
			echo "<option value=\"$adresaci[0]\">$adresaci[1]</option>";
		}
	}
	echo "</select></td></tr>";
	echo "<tr><td>Treść</td><td><textarea rows=\"3\" style=\"width:97%\" name=\"tresc\" form=\"ed_rek\">$wiersz[4]</textarea></td></tr>";
	echo "<tr><td>Uwagi</td><td><textarea rows=\"3\" style=\"width:97%\" name=\"uwagi\" form=\"ed_rek\">$wiersz[5]</textarea></td></tr>";
	echo "<tr><td colspan=\"2\" style=\"text-align:center\"><input type=\"submit\" name=\"czy\" value=\"Zapisz\" form=\"ed_rek\"/></td></tr>";
	echo "</table>";
}

?>

</body>
</html>

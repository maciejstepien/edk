<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Edycja adresata</title>
	<link type="text/css" rel="stylesheet" href="../css/okienko.css"/>
	<script type="text/javascript" src="../js/skrypty.js"></script>
</head>
	<body>
<?php
//Zabiezpieczenie przed dostępem do okienka bez uwierzytelnienia (zalogowania)
if (!isset($_SESSION['token'])) {
	echo "Aby edytować adresata, musisz być zalogowany.";
} else {
	//Nawiązanie połączenia z bazą MySQL
	require "../config/db.php"; //dane logowania do bazy MySQL
	try {
		$pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
		
	} catch (PDOException $e) {
		echo 'Połączenie nieudane: ' . $e->getMessage();
		exit;
	}
	$pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
	if (isset($_POST['czy'])) {
		extract($_POST);
		$zapytanie = $pol->prepare("UPDATE " . sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci " . "
									SET nazwa=?, miejsc=?, ulica=?, kod=?, tel=?, mail=?, dod_info=? 
									WHERE nr=?");
		$zapytanie->execute(array($nazwa,$miejsc,$ulica,$kod,$tel,$mail,$dod_info,$_GET['nr']));
		if ($zapytanie->errorCode() != '00000') {
			$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść poniższego komunikatu:\n';
			$sql_kom .= implode('\n', $zapytanie->errorInfo());
			echo '<script>alert("' . $sql_kom . '");</script>';
		} else {
			//echo '<script>window.opener.location.reload();window.close();</script>';
			//echo "<script>window.opener.location.search='?strona=ksiazka&adrNazwaEd=$nazwa';window.close();</script>";
			echo "<script>opener.document.getElementById('adrEdNazwa').value='" . addslashes($nazwa) . "';";
			echo "opener.document.getElementById('edytuj_rek').submit();window.close();</script>";
		}
	}
	//Pobranie danych adresata z bazy
	$zapytanie = $pol->prepare("SELECT nazwa, miejsc, ulica, kod, tel, mail, dod_info FROM " . sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci " . "WHERE nr=?");	
	$zapytanie->execute(array($_GET['nr']));
	$wynik = $zapytanie->fetch(PDO::FETCH_NUM);
	$wynik = array_map("htmlspecialchars", $wynik);
	?>
		<form method="post" id="edytuj_adr" onSubmit="return czyPuste('Nazwa')"></form>
		<p style="text-align:center">Edycja adresata "<?php echo $wynik[0]; ?>"</p>
		<p class="podpowiedz">Pamiętaj, że edycja adresata wpłynie na wszystkie istniejące już w dzienniku wpisy z jego udziałem.<br/>Jeśli chcesz tego uniknąć, dodaj nowego adresata zamiast edytować starego.</p>
		<table class="okienkowa">
			<tr>
				<td>Nazwa*</td>
				<td><input type="text" name="nazwa" id="Nazwa" style="width:100%" form="edytuj_adr" value="<?php echo $wynik[0]; ?>"/></td>
			</tr>
			<tr>
				<td>Miejscowość</td>
				<td><input type="text" name="miejsc" style="width:100%" form="edytuj_adr" value="<?php echo $wynik[1]; ?>"/></td>
			</tr>
			<tr>
				<td>Ulica i numer</td>
				<td><input type="text" name="ulica" style="width:100%" form="edytuj_adr" value="<?php echo $wynik[2]; ?>"/></td>
			</tr>
			<tr>
				<td>Kod</td>
				<td><input type="text" name="kod" size="6" maxlength="6" form="edytuj_adr" value="<?php echo $wynik[3]; ?>"/></td>
			</tr>
			<tr>
				<td>Telefon</td>
				<td><input type="text" name="tel" style="width:100%" form="edytuj_adr" value="<?php echo $wynik[4]; ?>"/></td>
			</tr>
			<tr>
				<td>e-mail</td>
				<td><input type="text" name="mail" style="width:100%" form="edytuj_adr" value="<?php echo $wynik[5]; ?>"/></td>
			</tr>
			<tr>
				<td>Informacje dodatkowe</td>
				<td><input type="text" name="dod_info" style="width:100%" form="edytuj_adr" value="<?php echo $wynik[6]; ?>"/></td>
			</tr>
			<tr>
				<td colspan="2" style="text-align:center"><input type="submit" name="czy" value="Zapisz" form="edytuj_adr" /></td>
			</tr>
		</table>
		<br/>
		<p style="font-size:10px;text-align:center">Pola oznaczone gwiazdką (*) są obowiązkowe.</p>
<?php
}
?>

</body>
</html>

<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8"/>
	<title>Dziennik korespondencji - zestawienie za okres: <?php echo $_POST['okres']; ?></title>
	<link type="text/css" rel="stylesheet" href="../css/eksport.css"/>
</head>
<body>
<div class="srodek">
	<button id="przycisk" class="ukryj" onClick="window.print()">Drukuj</button>
</div>
<p class="podpowiedz ukryj">Aby uzyskać prawidłowy wydruk,<br/>należy ustawić poziomą orientację drukarki</p>
	<p style="font-size:17px;text-align:center">DZIENNIK KORESPONDENCJI</p>
<div class="srodek2">
	Zestawienie za okres: <span style="font-weight:bold"><?php echo $_POST['okres']; ?></span><br/>
</div>
<table>
	<thead>
		<tr style="border:1px solid">
			<th style="width:40px">nr</th><th style="width:85px">data</th><th style="width:185px">nadawca</th><th style="width:185px">odbiorca</th><th style="width:250px">treść</th><th>uwagi</th>
		</tr>
	</thead>
<?php
if (isset($_POST) && $_POST['zapyt_eks'] != '') {
	//Nawiązanie połączenia z bazą MySQL
	require "../config/db.php"; //dane logowania do bazy MySQL
	try {
		$pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
	} catch (PDOException $e) {
		echo 'Connection failed: ' . $e->getMessage();
		exit;
	}
	$pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
	$pol->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); //bez tego jest błąd wynikający z tego, że $ost_wpisy to string
	$wynik = $pol->prepare($_POST['zapyt_eks']);
	$wynik->execute(unserialize($_POST['parametry']));
	$wynik->setFetchMode(PDO::FETCH_NUM);
	foreach($wynik as $wiersz) {
		if ($wiersz[6] == 1) {
			if ($_POST['czyUkryc'] == 'nie') {
				$klasa = ' class="schowEksport"';
			} else continue;
		} else {
			$klasa = '';
		}
		echo "\t<tr$klasa>\n\t\t<td>$wiersz[0]</td>\n\t\t<td>$wiersz[1]</td>\n";
		if ($wiersz[2] == 'o') {
			echo "\t\t<td>$wiersz[3]</td>\n\t\t<td></td>\n";
		} else {
			echo "\t\t<td></td>\n\t\t<td>$wiersz[3]</td>\n";
		}
	
		echo "\t\t<td>$wiersz[4]</td>\n\t\t<td>$wiersz[5]</td>\n";
	}
}
?>
</table>
<div class="srodek">
Wygenerowano: <?php echo date("d-m-o G:i"); ?>
</div>
</body>
</html>

<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
?>
<!DOCTYPE html>
<html  lang='pl'>
<head>
	<meta charset="utf-8"/>
	<link type="text/css" rel="stylesheet" href="css/login.css"/>
<?php
if (!isset($_SESSION['token'])) {
	require 'config/auth.php';
	$gClient = new Google_Client();
	$gClient->setApplicationName('Zaloguj do dziennika korespondencji TPBA');
	$gClient->setClientId($google_client_id);
	$gClient->setClientSecret($google_client_secret);
	$gClient->setRedirectUri($google_redirect_url);
	$google_oauthV2 = new Google_Oauth2Service($gClient);
	$authUrl = $gClient->createAuthUrl();
	if (!isset($_GET['code'])) {
?>
	<title>Logowanie do EDK</title>
</head>
<body>
<p style="position:absolute;top:3px;right:40px;font-size:12px;font-style:italic"><a href="content/testowanie.php" target="_blank">Testowanie aplikacji dla kont<br/>spoza domeny tpba.pl lub<br/>nieuprawnionych</a></p>
<div id="logowanie">
	<p id="powitanie">
		Witaj w Elektronicznym Dzienniku Korespondencji<br/>Towarzystwa Pomocy im. św. Brata Alberta
	</p>
	Zaloguj się, by móc korzystać z dziennika.<br/>
	<span id="podpowiedz">Użyj swojego konta w domenie tpba.pl.<br/></span>
	<a href="<?php echo $authUrl; ?>" target="_self"><button id="przycisk">Przejdź do logowania</button></a>
</div>

<?php
	} else { //jeśli istnieje $_GET['code'];
		$gClient->authenticate($_GET['code']);
		$user = $google_oauthV2->userinfo->get();
		//Nawiązanie połączenia z bazą MySQL
		require "config/db.php"; //dane logowania do bazy MySQL
		try {
			$pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
		} catch (PDOException $e) {
			echo 'Połączenie nieudane: ' . $e->getMessage();
			exit;
		}
		$pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
		//check if user's email exists in uzyt_plac table
		$zapytanie = $pol->prepare("SELECT COUNT(email) FROM uzyt_plac WHERE email = ?");
		$zapytanie->execute(array($user['email']));
		if (!$zapytanie->fetchColumn(0)) {
			//session_destroy();
			echo '<script>alert("Nie masz uprawnień do korzystania z EDK.\nSpróbuj zalogować się na inne konto.");</script>';
		}else{ //Użytkownik ma uprawnienia do EDK. Wpisujemy jego dane oraz token do sesji
			$_SESSION['token'] = $gClient->getAccessToken();
			$_SESSION['userid'] = $user['id'];
			$_SESSION['usermail'] = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
			$_SESSION['username'] = $user['name'];
		}
		echo "<script>location.href='" . filter_var($google_redirect_url, FILTER_SANITIZE_URL) . "'</script>";
	}
	die("\n</body>\n</html>"); //to po to, by nie ładowała się reszta strony z klasy csite! (która ładuje przegladaj.php)
}else{
?>
	<title>Dziennik korespondencji</title>
	<link type="text/css" rel="stylesheet" href="css/style.css"/>
	<link rel="stylesheet" type="text/css" media="all" href="js/jsdatepick-calendar/jsDatePick_ltr.min.css"/>
	<script type="text/javascript" src="js/skrypty.js"></script>
	<script type="text/javascript" src="js/jsdatepick-calendar/jsDatePick.min.1.3.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</head>
<?php
	/*Ustawienie polskich nazw dla dat. 
	UWAGA: lokal pl_PL.utf8 (lub podobny) musi być na serwerze! Sprawdzenie przez komendę "locale -a"*/
	setlocale(LC_TIME, 'pl_PL.utf8');
	$dkN = array_fill(0, 4, "");
	if ($_GET['strona'] == 'dodaj') {
		if ($_GET['typ_kor'] == 'o')
			$dkN[0] = " wcis"; //dodatkowa klasa Nagłówka w CSS
		elseif ($_GET['typ_kor'] == 'w')
			$dkN[1] = " wcis";
	} elseif ($_GET['strona'] == 'ksiazka') {
		$dkN['3'] = " wcis"; 
	} else {
		$dkN['2'] = " wcis"; //domyślnie (dla czystego adresu ./index.php) oraz dla $_GET['przegladaj'] włącza się to [chodzi o kolor zakładki]
	}
?>
<body>
	<div id="komunikat"></div>
	<div id="header">
		<a href="index.php?strona=dodaj&typ_kor=o"><li class="header<?php echo $dkN[0]; ?>">Dodaj wpis<br/>ODEBRANE</li></a>
		<a href="index.php?strona=dodaj&typ_kor=w"><li class="header<?php echo $dkN[1]; ?>">Dodaj wpis<br/>WYSŁANE</li></a>
		<a href="index.php?strona=przegladaj"><li class="header<?php echo $dkN[2]; ?>">Przeglądaj<br/>dziennik</li></a>
		<a href="index.php?strona=ksiazka"><li class="header<?php echo $dkN[3]; ?>">Książka<br/>adresowa</li></a>
	</div>
	<!--<div id="pole">-->
<?php
require_once "./libs/google-login-api/g-login.php";
echo "\n";
?>
	<!--</div>-->
<?php
} //koniec elsa z początku pliku
?>

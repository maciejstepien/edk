<?php
//Usuwanie rekordu
if (isset($_POST['nr_do_us'])) {
	$zapytanie = $pol->prepare("DELETE FROM " . sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci " . "
								WHERE nr=?");
	$zapytanie->execute(array($_POST['nr_do_us']));
	//Obsługa błędu
	if ($zapytanie->errorCode() === '00000') {
		echo "<script>document.getElementById('komunikat').innerHTML='Adresat \'" . addslashes(htmlspecialchars($_POST['adrNazwa'])) . "\' został usunięty.';</script>";
	} else {
		if ($zapytanie->errorCode() === '23000') {
			$sql_kom = 'UWAGA!\nNie można usunąć adresata, dla którego istnieje\nwpis w dzienniku. Zamiast tego edytuj adresata\nlub usuń odpowiedni wpis w dzienniku.';
		} else {
			$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść\nponiższego komunikatu:\n';
			$errorInfo = $zapytanie->errorInfo();
			$sql_kom .= $errorInfo[1] . ' (' . $errorInfo[0] . '):\n' . $errorInfo[2];
		}
		echo '<script>alert("' . $sql_kom . '");</script>';
	}
	
/*	
	if ($zapytanie->errorCode() != '00000') {
		$bladString = http_build_query($zapytanie->errorInfo(), 'blad_');
	} else {
		$bladString = 'blad_0=00000';
		//echo '<div id="komunikat"></div>';
		echo "<script>document.getElementById('komunikat').innerHTML='Adresat został usunięty.';</script>";
		die();
	}
	echo "<script>location.href='index.php?strona=ksiazka&$bladString';</script>";
} elseif (isset($_GET['blad_0'])) {
	if ($_GET['blad_0'] === '00000') {
		echo '<div id="komunikat">Adresat został usunięty.</div>';
	} else { //W przypadku błędu wyskakuje alert
		if ($_GET['blad_0'] == '23000') {
			$sql_kom = 'UWAGA!\nNie można usunąć adresata, dla którego istnieje\nwpis w dzienniku. Zamiast tego edytuj adresata\nlub usuń odpowiedni wpis w dzienniku.';
		} else {
			$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść\nponiższego komunikatu:\n';
			$sql_kom .= implode('\n', array($_GET['blad_0'],$_GET['blad_1'],$_GET['blad_2']));
		}
		echo '<script>alert("' . $sql_kom . '");</script>';
	}
	*/
}
//Komunikat po powodzeniu dodawania adresata
if (isset($_POST['adrDodNazwa'])) {
	//echo '<div id="komunikat">';
	echo "<script>document.getElementById('komunikat').innerHTML = 'Adresat \'" . addslashes(htmlspecialchars($_POST['adrDodNazwa'])) . "\' został dodany do książki adresowej.';</script>";
	//echo '</div>';
	//echo '<script>$("#komunikat").delay(5000).fadeOut(1000);</script>';
}
//Komunikat po powodzeniu edycji adresata
if (isset($_POST['adrEdNazwa'])) {
	/*echo '<div id="komunikat">';
	echo "Edycja adresata '{$_GET['adrEdNazwa']}' zakończona powodzeniem.";
	echo '</div>';
	echo '<script>$("#komunikat").delay(5000).fadeOut(1000);</script>';*/
	echo "<script>document.getElementById('komunikat').innerHTML = 'Edycja adresata \'" . addslashes(htmlspecialchars($_POST['adrEdNazwa'])) . "\' zakończona powodzeniem.';</script>";
}
?>
<!--
<form method="get" id="ks_akcja">
	<input type="hidden" name="strona" value="ksiazka" form="ks_akcja"/>
</form>
-->
<form method="post" id="usun_rek"> <!--usuwa rekord z bazy - tu: rekord książki adresowej-->
	<input type="hidden" name="strona" id="strona"/>
	<input type="hidden" name="nr_do_us" id="nr_do_us"/> 
	<input type="hidden" name="adrNazwa" id="adrNazwa"/> 
</form>
<form method="post" id="dodaj_rek"> <!--dodaje rekord do bazy - tu: rekord książki adresowej-->
	<input type="hidden" name="adrDodNazwa" id="adrDodNazwa"/> 
</form>
<form method="post" id="edytuj_rek"> <!--usuwa rekord z bazy - tu: rekord książki adresowej-->
	<input type="hidden" name="adrEdNazwa" id="adrEdNazwa"/> 
</form>
<div style="text-align:center;margin-top:20px;">
	<button onClick="popupwindow('./content/dodaj_adresata.php', 600, 470)">Dodaj nowego adresata</button>
</div>
<table id="wpisy">
	<thead>
		<tr>
			<th style="width:240px">NAZWA</th><th style="width:140px">MIEJSCOWOŚĆ</th><th style="width:180px">ULICA</th><th style="width:50px">KOD</th><th style="width:110px">TELEFON</th><th style="width:200px">E-MAIL</th><th style="width:170px">DOD. INFORMACJE</th><th style="width:60px"></th><th style="width:50px"></th>
		</tr>
	</thead>
<?php
$wynik = $pol->query("SELECT * FROM " . sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci " . "
						ORDER BY nazwa");
foreach($wynik as $wiersz) {
	$wiersz = array_map('htmlspecialchars',$wiersz);
	echo "<tr>
		<td>$wiersz[1]</td><td>$wiersz[2]</td><td>$wiersz[3]</td><td>$wiersz[4]</td><td>$wiersz[5]</td><td>$wiersz[6]</td><td>$wiersz[7]</td>";
		echo "\t\t<td style=\"text-align:center\">\n\t\t\t<button type=\"button\" style=\"font-size:10px\" onClick=\"popupwindow('./content/edytuj_adr.php?nr=$wiersz[0]', 600, 450)\">EDYTUJ</button>\n\t\t</td>\n";
		echo "\t\t<td style=\"text-align:center\">\n\t\t\t<button type=\"button\" style=\"color:red; font-size:10px\" onClick=\"usunRekord($wiersz[0], 'ksiazka','" . addslashes($wiersz[1]) . "')\">USUŃ</button>\n\t\t</td>\n";
		echo "\t</tr>\n";
}
?>
</table>


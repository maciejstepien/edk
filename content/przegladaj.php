<?php
//Usuwanie (skreślanie) rekordu
if (isset($_POST['nr_do_us'])) {
	//$pol->exec("DELETE FROM " . sprintf("%02d", $_SESSION['id_placowki']) . "_korespondencja " . "WHERE nr={$_GET['nr_do_us']}");
	$zapytanie = $pol->prepare("UPDATE " . sprintf("%02d", $_SESSION['id_placowki']) . "_korespondencja " . "
								SET czy_anulowano = 1 
								WHERE nr=?");
	$zapytanie->execute(array($_POST['nr_do_us']));
	if ($zapytanie->errorCode() === '00000') {
		echo "<script>document.getElementById('komunikat').innerHTML='Anulowano wpis nr {$_POST['nr_do_us']}.';</script>";
	} else {
		$sql_kom = 'WYSTĄPIŁ BŁĄD!\nSkontaktuj się z administratorem podając treść\nponiższego komunikatu.\n';
		$errorInfo = $zapytanie->errorInfo();
		$sql_kom .= $errorInfo[1] . ' (' . $errorInfo[0] . '):\n' . $errorInfo[2];
		echo '<script>alert("' . $sql_kom . '");</script>';
	}
}
//Komunikat po powodzeniu edycji wpisu
if (isset($_POST['wpisEdNr'])) {
	echo "<script>document.getElementById('komunikat').innerHTML = 'Edycja wpisu nr {$_POST['wpisEdNr']} zakończona powodzeniem.';</script>";
}
extract($_GET);
//Część wspólna zapytania SQL (połączenie tablic "korespondencja" i "adresaci"). Przy czy_anulowano musi być +0, inaczej mysql nie wyświetla poprawnie wartości typu bit
$zapyt = "SELECT k.nr, DATE_FORMAT(k.data, '%d-%m-%Y') 
			AS data, k.typ_kor, a.nazwa, k.tresc, k.uwagi, k.czy_anulowano+0 
			FROM " . sprintf("%02d", $_SESSION['id_placowki']) . "_korespondencja " . "AS k 
			LEFT JOIN " . sprintf("%02d", $_SESSION['id_placowki']) . "_adresaci " . "AS a 
			ON k.nad_odb=a.nr";

if (isset($ost_wpisy) && $ost_wpisy > 0) {
	$pol->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); //bez tego jest błąd wynikający z tego, że $ost_wpisy to string
	$wynik = $pol->prepare("(SELECT * FROM ($zapyt) AS robocza ORDER BY nr DESC LIMIT ?) ORDER BY nr ASC");
	$parametry = array($ost_wpisy);
}

if (isset($ost_dni) && $ost_dni > 0) {
	$wynik = $pol->prepare("SELECT * FROM ($zapyt) AS robocza WHERE STR_TO_DATE(data,'%d-%m-%Y') > CURDATE() - INTERVAL ? DAY ORDER BY nr ASC");
	$parametry = array($ost_dni);
}

if (isset($miesiac) && isset($rok) && $miesiac > 0 && $rok > 0) {
	$wynik = $pol->query("SELECT * FROM ($zapyt) AS robocza WHERE MONTH(STR_TO_DATE(data, '%d-%m-%Y')) = $miesiac AND YEAR(STR_TO_DATE(data, '%d-%m-%Y')) = $rok ORDER BY nr ASC", PDO::FETCH_NUM);//->fetchAll();
	$wynik = $pol->prepare("SELECT * FROM ($zapyt) AS robocza 
								WHERE MONTH(STR_TO_DATE(data, '%d-%m-%Y')) = ? AND YEAR(STR_TO_DATE(data, '%d-%m-%Y')) = ? 									ORDER BY nr ASC");
	//$array = array($miesiac,$rok);
	$parametry = array($miesiac,$rok);
}

if (isset($data_od)) {
	$wynik = $pol->prepare("SELECT * FROM ($zapyt) AS robocza 
							WHERE STR_TO_DATE(data, '%d-%m-%Y') >= STR_TO_DATE(?, '%d-%m-%Y') 
							AND STR_TO_DATE(data, '%d-%m-%Y') <= STR_TO_DATE(?, '%d-%m-%Y') 
							ORDER BY nr ASC");
	$parametry = array($data_od,$data_do);
}

if (isset($szukaj) && $szukaj !== '') {
	switch ($gdzie) {
		case 'nr':
			$wynik = $pol->prepare("SELECT * FROM ($zapyt) AS robocza WHERE nr = ?");
			$parametry = array($szukaj);
			break;
		case 'tresc_uwagi';
			$wynik = $pol->prepare("SELECT * FROM ($zapyt) AS robocza WHERE tresc LIKE ? OR uwagi LIKE ?");
			$parametry = array("%$szukaj%","%$szukaj%");
			break;
		default: //gdy $gdzie = 'tresc' lub $gdzie = 'uwagi', lub $gdzie = 'nazwa'
			$wynik = $pol->prepare("SELECT * FROM ($zapyt) AS robocza WHERE $gdzie LIKE ?");
			$parametry = array("%$szukaj%");
	}
}

?>
<form method="get" id="ostatnie_wpisy"></form>
<form method="get" id="ostatnie_dni"></form>
<form method="get" id="zakres"></form>
<form method="get" id="miesiac"></form>
<form method="get" id="szukaj_f"></form>
<form method="post" id="usun_rek"> 
	<input type="hidden" name="strona" id="strona"/>
	<input type="hidden" name="nr_do_us" id="nr_do_us"/> 
</form>
<form method="post" id="edytuj_rek"> 
	<input type="hidden" name="wpisEdNr" id="wpisEdNr"/> 
</form>
<form method="post" action="./content/eksport.php" id="eksport" target="_blank"></form>
<div class="szukaj" id="przegladaj">
	<table id="szukaj_t">
	<tr>
		<td style="border-bottom:2px solid white">ostatnich 
			<select name="ost_wpisy" form="ostatnie_wpisy" style="width:48px" >
				<option value=""></option>
<?php
	for ($i = 10; $i <= 50; $i += 10) {
		echo "\t\t\t\t<option value=\"$i\"";
		if (isset($ost_wpisy) && $ost_wpisy == $i) echo " selected";
		echo ">$i</option>\n";		
	}
?>
				<option value="100">100</option>
			</select> wpisów
		</td>
		<td style="border-right:2px solid white; border-bottom:2px solid white">
			<input type="submit" name="czy_wysl" value="Pokaż" form="ostatnie_wpisy"/>
		</td>
		<td style="border-bottom:2px solid white">miesiąc 
			<select name="miesiac" form="miesiac">
				<option value=""></option>
<?php
	for ($i = 1; $i<=12; $i++) {
		echo "\t\t\t\t<option value=\"$i\"";
		if (isset($_GET['miesiac']) && $_GET['miesiac'] == $i) {
			echo " selected";
		}
		echo ">" . strftime('%B',strtotime("22-$i-2008")) . "</option>\n";
	}
?>
			</select>
			<select name="rok" form="miesiac">
<?php
	for ($i = 2014; $i<=2020; $i++) {
		echo "\t\t\t\t<option value=\"$i\"";
		if (isset($_GET['rok']) && $_GET['rok'] == $i) {
			echo " selected";
		} elseif (!isset($_GET['rok']) && $i == date("Y")) {
			echo " selected";
		}
		echo ">$i</option>\n";
	}
?>
			</select>
		</td>
		<td style="border-bottom:2px solid white;">
			<input type="submit" name="czy_wysl" value="Pokaż" form="miesiac"> 
		</td>
		<td style="width:150px">
		</td>
		<td style="padding-right:0px">
				<select style="width:105px" name="gdzie" form="szukaj_f">
					<option value="nazwa" <?php if (isset($_GET['gdzie']) && $_GET['gdzie'] == 'nazwa') echo "selected"; ?>>adresaci</option>
					<option value="nr" <?php if (isset($_GET['gdzie']) && $_GET['gdzie'] == 'nr') echo "selected"; ?>>nr wpisu</option>
					<option value="tresc" <?php if (isset($_GET['gdzie']) && $_GET['gdzie'] == 'tresc') echo "selected"; ?>>treść</option>
					<option value="uwagi" <?php if (isset($_GET['gdzie']) && $_GET['gdzie'] == 'uwagi') echo "selected"; ?>>uwagi</option>
					<option value="tresc_uwagi" <?php if (isset($_GET['gdzie']) && $_GET['gdzie'] == 'tresc_uwagi') echo "selected"; ?>>treść i uwagi</option>
				</select>
		</td>
		<td style="padding-left:0px;padding-right:0px">
				<input type="search" name="szukaj" style="width:240px" form="szukaj_f" placeholder="Szukaj w dzienniku" value="<?php if (isset($_GET['szukaj'])) echo $_GET['szukaj']; ?>"/>
		</td>
		<td style="text-align:right;padding-left:0px;padding-right:0px">
				<input type="submit" value="Szukaj" form="szukaj_f"/>
		</td>
	</tr>
	<tr>
		<td>ostatnich 
			<input type="number" name="ost_dni" style="width:40px" min="1" form="ostatnie_dni" value="<?php if (isset($_GET['ost_dni'])) echo $_GET['ost_dni']; ?>"/> dni
		</td>
		<td style="border-right: 2px solid white">
			<input type="submit" name="czy_wysl" value="Pokaż" form="ostatnie_dni">
		</td>
		<td>
			od <input type="text" style="width:80px" name="data_od" id="inputField" form="zakres" value="<?php if (isset($_GET['data_od'])) echo $_GET['data_od']; ?>"/>
			do <input type="text" style="width:80px" name="data_do" id="inputField2" form="zakres" value="<?php if (isset($_GET['data_do'])) echo $_GET['data_do']; elseif(!isset($_GET['czy_wysl'])) echo date('d-m-Y') ?>"/>
		</td>
		<td>
			<input type="submit" value="Pokaż" form="zakres"/>
		</td>
		<td colspan="4">
		</td>
	</tr>
	<tr>
		<td colspan="4" style="text-align:right;padding-top:0px">
			<input type="submit" value="Drukuj zestawienie" form="eksport"/>
		</td>
		<td colspan="4" style="text-align:right;padding-top:0px">
			<input type="button" id="guzik" onClick='schowajWiersze()' value="Schowaj anulowane wpisy"/>
		</td>
	</tr>
	</table>
</div>
<?php
if (isset($wynik)) {
	if (!$wynik) {
		echo "\nPDO::errorInfo():\n";
		print_r($pol->errorInfo());
	} 
?>
<table id="wpisy">
<thead>
	<tr>
		<th style="width:40px">NR</th>
		<th style="width:80px">DATA</th>
		<th style="width:215px">NADAWCA</th>
		<th style="width:215px">ODBIORCA</th>
		<th style="width:300px">TREŚĆ</th>
		<th style="width:235px">UWAGI</th>
		<th style="width:55px"></th>
		<th style="width:60px"></th>
	</tr>
</thead>
<?php
	$wynik->execute($parametry);
	$wynik->setFetchMode(PDO::FETCH_NUM);
	foreach($wynik as $wiersz) {
		if ($wiersz[6] == 1) {
			$klasa = ' class="schow"';
			$styl = 'visibility:hidden';
		} else {
			$klasa = '';
			$styl = '';
		}
		$wiersz = array_map('htmlspecialchars',$wiersz);
		echo "\t<tr$klasa>\n\t\t<td>$wiersz[0]</td>\n\t\t<td>$wiersz[1]</td>\n";
		if ($wiersz[2] == 'o') {
			echo "\t\t<td>$wiersz[3]</td>\n\t\t<td></td>\n";
		} else {
			echo "\t\t<td></td>\n\t\t<td>$wiersz[3]</td>\n";
		}
	
		echo "\t\t<td>$wiersz[4]</td>\n\t\t<td>$wiersz[5]</td>\n";
		echo "\t\t<td style=\"text-align:center\">\n\t\t\t<button type=\"button\" style=\"font-size:10px;$styl\" onClick=\"popupwindow('./content/edytuj.php?nr=$wiersz[0]', 600, 450)\">EDYTUJ</button>\n\t\t</td>\n";
		echo "\t\t<td style=\"text-align:center\">\n\t\t\t<button type=\"button\" style=\"color:red;font-size:10px;$styl\" onClick=\"usunRekord($wiersz[0], 'przegladaj')\">ANULUJ</button>\n\t\t</td>\n";
		echo "\t</tr>\n";
	}
	echo '</table>';
//Tworzenie zmiennej z okresem zestawienia, do wykorzystania na wydruku
	if (isset($ost_wpisy)) {
		$okres = "$ost_wpisy ostatnich wpisów";
	} elseif (isset($ost_dni)) {
		$okres = "$ost_dni ostatnich dni";
	} elseif (isset($data_od)) {
		$okres = "$data_od do $data_do";
	} elseif (isset($miesiac)) {
		$okres = strtoupper(strftime('%B',strtotime("22-$miesiac-2008"))) . " $rok"; //bierzemy bez _submit, bo chodzi o datę do pokazania na wydruku, czyli dd-mm-yyyy
	}
	$zapyt_eks = $wynik->queryString; //to trzeba przekazać do nowej stronki (to jest samo zapytanie, które trzeba wykonać ponownie)
}
?>
<input type="hidden" name="okres" value="<?php if (isset($okres)) echo $okres; ?>" form="eksport"/>
<!--eksport zapytania SQL-->
<input type="hidden" name="zapyt_eks" value="<?php if (isset($zapyt_eks)) echo $zapyt_eks; ?>" form="eksport"/>
<input type="hidden" name="parametry" value="<?php if (isset($parametry)) echo htmlspecialchars(serialize($parametry)); ?>" form="eksport"/>
<input type="hidden" name="czyUkryc" value="nie" id="czyUkryc" form="eksport"/>

<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Resetowanie numeracji</title>
	<link type="text/css" rel="stylesheet" href="../css/okienko.css"/>
	<script type="text/javascript" src="../js/skrypty.js"></script>
</head>
<body>
<?php
//Zabiezpieczenie przed dostępem do okienka bez uwierzytelnienia (zalogowania)
if (!isset($_SESSION['token'])) {
    echo "Aby zresetować numerację, musisz być zalogowany.";
} else {
    //Nawiązanie połączenia z bazą MySQL
    require "../config/db.php"; //dane logowania do bazy MySQL
    try {
        $pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
        $pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
    } catch (PDOException $e) {
        echo 'Połączenie nieudane: ' . $e->getMessage();
        exit;
    }
    $pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
?>

	
	
<?php
}
?>
</body>
</html>
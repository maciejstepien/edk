<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
?>
<!doctype HTML>
<html>
<head>
    <meta charset="utf-8"/>
    <title>Zarządzanie EDK</title>
    <link type="text/css" rel="stylesheet" href="../css/zarzadzanie.css"/>
    <script type="text/javascript" src="../js/zarzadzanie.js"></script>
    <script type="text/javascript" src="../js/skrypty.js"></script>
</head>
<body>
<?php
if (!isset($_SESSION['token'])) {
    echo '<script>alert("Nie możesz korzystać z panelu zarządzania, ponieważ nie jesteś zalogowany.");window.location = "..";</script>';
} else {
    echo '<div id="pole">';
    echo "<b>{$_SESSION['usermail']}</b><br/>\n{$_SESSION['username']}<br/>";
    echo '<a href="?reset=1">Wyloguj</a>';
    echo '</div>';

    //Nawiązanie połączenia z bazą MySQL
    require "../config/db.php"; //dane logowania do bazy MySQL
    try {
        $pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
    } catch (PDOException $e) {
        echo 'Połączenie nieudane: ' . $e->getMessage();
        exit;
    }
    $pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
    if (isset($_GET['nazwaPlacowki'])) {
        $zapytanie = $pol->prepare("UPDATE placowki SET nazwa_placowki = ? WHERE id = ?");
        $zapytanie->execute(array($_GET['nazwaPlacowki'],$_GET['idPlacowki']));
    }
    $uzytkownicy = $pol->query("SELECT up.email, group_concat( p.nazwa_placowki ORDER BY p.nazwa_placowki SEPARATOR '<br/>' )
                                                                FROM uzyt_plac up
                                                                LEFT JOIN placowki p ON up.id_placowki = p.id
                                                                GROUP BY up.email", PDO::FETCH_NUM);
    $placowki = $pol->query("SELECT * FROM placowki", PDO::FETCH_NUM);
?>
<div class="srodek">
	<h3>Zarządzanie użytkownikami i placówkami</h3>
	Koło Gliwickie
</div>
<div style="height:30px;margin-top:20px">
    <button class="lewa" onClick="popupwindow('dodaj_uzyt.php', 600, 450)">Dodaj nowego użytkownika</button>
<button class="prawa" onClick="popupwindow('dodaj_plac.php', 300, 300)">Dodaj nową placówkę</button>
</div>
<form method="get" id="nazwaPlacForm">
	<input type="hidden" name="nazwaPlacowki" id="nazwaPlacowki" value="" form="nazwaPlacForm"/>
	<input type="hidden" name="idPlacowki" id="idPlacowki" value="" form="nazwaPlacForm"/>
</form>
<table class="rekordy lewa">
    <tr>
        <th style="width:250px">UŻYTKOWNIK</th><th style="width:150px">TYP KONTA</th><th>OBSŁUGIWANE PLACÓWKI</th>
    </tr>
<?php
    foreach ($uzytkownicy as $uzytkownik) {
        echo "<tr>\n";
        echo "<td>$uzytkownik[0]</td><td>ni mo<br/><a href=\"\" style=\"float:right\">Zmień</a></td><td>$uzytkownik[1]<br/><a href=\"\" style=\"float:right\">Zmień</a></td>\n";
        echo "</tr>\n";
    }
?>
</table>
<table class="rekordy prawa">
    <tr>
        <th style="width:280px">NAZWA PLACÓWKI</th><th style="width:55px;"></th><th style="width:45px;"></th>
    </tr>
<?php
    foreach ($placowki as $placowka) {
        echo "<tr>\n";
        echo "<td>$placowka[1]</td><td style=\"text-align:center\"><input type=\"button\" class=\"button\" value=\"EDYTUJ\" onClick=\"zmienNazwe('$placowka[1]',$placowka[0])\"></td><td style=\"text-align:center\"><input type=\"button\" class=\"button\" style=\"color:red\" value=\"USUŃ\"></td>\n";
        echo "</tr>\n";
    }
?>
</table>
<button style="display:block;clear:both;position:relative;top:100px" class="lewa" onClick="popupwindow('reset_numeracji.php', 600, 450)">Reset numeracji dziennika</button>
<?php
} //koniec elsa z warunku zalogowania
?>
</body>
</html>

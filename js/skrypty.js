//Wykorzystanie: dodaj.php, przegladaj.php, ksiazka.php
//Funkcja: wyskakujące okienko o zadanych rozmiarach
function popupwindow(url, w, h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	return window.open(url, '', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
} 

//Wykorzystanie: przegladaj.php, ksiazka.php
//Funkcja: usuwanie rekordu z bazy (tabeli korespondencji lub książki adresowej)
function usunRekord(js_nr, js_strona, js_nazwa) { 
	var pytanie;
	//var adres = window.location.href;
	//var kwerenda = window.location.search;
	if (js_strona == 'przegladaj') {
		pytanie = "Czy na pewno anulować wpis nr " + js_nr + "?\nZostanie on skreślony i nie będzie już możliwa jego edycja.";
	} else {
		pytanie = "Czy na pewno usunąć adresata '" + js_nazwa + "'?";
	}
    if (confirm(pytanie) == true) {
    	document.getElementById("nr_do_us").value = js_nr;
    	//Sprawdzam czy zmienna istnieje (js_nazwa jest parametrem opcjonalnym, używanym tylko w ksiazka.php)
    	if (typeof js_nazwa === 'string') {
    		document.getElementById("adrNazwa").value = js_nazwa;
    	}
    	document.getElementById("usun_rek").submit();
    }
}

//Wykorzystanie: przegladaj.php
//Funkcja: wybór zakresu dat do przeglądania
window.onload = function(){
	new JsDatePick({
		useMode:2,
		target:"inputField",
		dateFormat:"%d-%m-%Y"
	});
	new JsDatePick({
		useMode:2,
		target:"inputField2",
		dateFormat:"%d-%m-%Y"
	});
};

//Wykorzystanie: dodaj.php, dodaj_adresata.php
//Funkcja: sprawdzenie czy pole jest puste
//Argumenty: są nimi kolejne id sprawdzanych pól formularza. Id powinny mieć ludzką postać, gdyż używane są też w alercie jako nazwa pola.
function czyPuste() {
	//document.write(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4],arguments[5]);
	//document.write('tu: '+document.getElementById(arguments[4]).value);
	//document.write(arguments.length/2);
    for (i = 0; i < arguments.length; i++) {
        var x = document.getElementById(arguments[i]).value;
        if (x == null || x == "") {
            alert('Pole "' + arguments[i] + '" nie może być puste.');
            return false; 
        }
    }
    /*var x = document.getElementById(js_id).value;
    if (x == null || x == "") {
        alert('Pole "' + js_nazwaPola + '" nie może być puste.');
        return false;
    }*/
    
}

//Wykorzystanie: przegladaj.php, eksport.php
//Funckja: chowanie wierszy tabeli
function schowajWiersze() {
		if (document.getElementById("guzik").value == "Schowaj anulowane wpisy") {
			$( '.schow' ).hide();
			document.getElementById("guzik").value = "Pokaż anulowane wpisy";
			document.getElementById("czyUkryc").value = 'tak'; //To jest dla $_POST - by przekazać do pliku eksport.php
		} else {
			$( '.schow' ).show();
			document.getElementById("guzik").value = "Schowaj anulowane wpisy";
			document.getElementById("czyUkryc").value = 'nie';
		}
	//return document.getElementById("schow").style.display;
}

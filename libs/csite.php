<?php
    class csite {
        private $headers;
        private $footers;
        private $page;
        private $header;
        private $footer;
        private $headerFile;
        private $pageFile;
        private $footerFile;

        public function __construct($headerFile, $pageFile, $footerFile) {
            //$this->headers = array();
            //$this->footers = array();
            //$this->header = file_get_contents("./content/header.php");
            //$this->footer = file_get_contents("./content/footer.php");
            $this->headerFile = $headerFile;
            $this->pageFile = $pageFile;
            $this->footerFile = $footerFile;
        }

        public function __destruct() {
            // clean up here
        }

        public function render() {
            //foreach($this->headers as $header) {
                include $this->headerFile;
                //echo $this->header;
            //}

            //$this->page->render();
            include $this->pageFile;

            //foreach($this->footers as $footer) {
                include $this->footerFile;
                //echo $this->footer;
            //}
        }

        public function addHeader($file) {
            $this->header = $file;
        }

        public function addFooter($file) {
            $this->footer = $file;
        }

        public function setPage(cpage $page) {
            $this->page = $page;
        }
    }
?>

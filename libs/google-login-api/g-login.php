<?php
if (isset($_REQUEST['reset'])) {
	//$gClient->revokeToken();
	session_destroy();
	echo "<script>location.reload();</script>";
}
//Nawiązanie połączenia z bazą MySQL
require "config/db.php"; //dane logowania do bazy MySQL
try {
	$pol = new PDO("mysql:host=$host;port=$port;dbname=$baza;charset=utf8", $uzytkownik, $haslo);
} catch (PDOException $e) {
	echo 'Połączenie nieudane: ' . $e->getMessage();
	exit;
}
$pol->exec("set names utf8"); //dla PHP<5.3.6, bo ignoruje parametr charset z konstruktora PDO
?>
<div id="pole">
<?php
echo "<b>{$_SESSION['usermail']}</b><br/>\n{$_SESSION['username']}<br/>";
?>
<a style="color:#CC6600" href="content/zarzadzanie.php" target="_blank">ZARZĄDZANIE</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<a href="?reset=1">Wyloguj</a>
</div>
<div id="placowka">
<?php
//Wypisywanie placówek użytkownika
$placowki = $pol->prepare("SELECT p.id, p.nazwa_placowki 
							FROM placowki p 
							RIGHT JOIN uzyt_plac up ON p.id=up.id_placowki 
							WHERE up.email=?");
$placowki->execute(array($_SESSION['usermail']));
if ($placowki->rowCount() == 1) {
	$placowka = $placowki->fetch(PDO::FETCH_NUM);
	$_SESSION['id_placowki'] = $placowka[0];
	echo "$placowka[1]<br/>";
}else{
?>
<form method="get" id="placowki"></form>
<select name="id_placowki" form="placowki" style="width:250px" onChange='this.form.submit()'>
<?php
	if (isset($_GET['id_placowki'])) {
		$_SESSION['id_placowki'] = $_GET['id_placowki'];
	}elseif(!isset($_SESSION['id_placowki'])) {
		echo "\t<option value=\"\" selected>----- wybierz placówkę -----</option>";
	}
	while ($placowka = $placowki->fetch(PDO::FETCH_NUM)) {
	 	if (isset($_SESSION['id_placowki']) && $placowka[0] == $_SESSION['id_placowki']) {
	 		echo "\n\t<option value=\"$placowka[0]\" selected>$placowka[1]</option>";
	 	}else{
	 		echo "\n\t<option value=\"$placowka[0]\">$placowka[1]</option>";
	 	}
	}
	echo "\n</select><br/>\n";
	
} //koniec elsa sprzed formularza
?>
</div> <!--koniec diva z placówkami-->
<?php
if (!isset($_SESSION['id_placowki']) || $_SESSION['id_placowki'] == "") {
	die("\n</body>\n</html>");
}
//echo '</body></html>';
?>

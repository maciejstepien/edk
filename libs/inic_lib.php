<?php
function __autoload($class) {
	include "$class.php";
}

$headerF = "./content/header.php";
$footerF = "./content/footer.php";
    
//Wybór strony z właściwą treścią (domyślnie):
if (!isset($_GET['strona'])) {
	$_GET['strona'] = 'przegladaj';
}

//Wybór strony z właściwą treścią (po kliknięciu na zakładkę):
$pageF = "./content/{$_GET['strona']}.php";

?>
